#IfWinActive, AION
#include classMemory.ahk
CoordMode, Mouse, Client
CoordMode, ToolTip, Client

global searchItem := "Holy Upgrade Serum" ;Not using that right now
global priceToBuy := "100000" ;Price to look for
global delay := 100

class aButton {
	__New(x, y) {
		this.X := x
		this.Y := y
	}
	
	Click() {
		MouseMove, this.X, this.Y
		click
	}
}

class Item {
	__New(x, y, x2, y2) {
		this.X := x
		this.Y := y
		this.X2 := x2
		this.Y2 := y2
	}
	OCR() {
		WinGet, hWnd, ID, A
		JEE_ClientToScreen(hWnd, 0, 0, clientX, clientY)
		result := StdOutToVar("Capture2Text\Capture2Text_CLI.exe --screen-rect """ this.X + clientX " " this.Y + clientY " " this.X2 + clientX " " this.Y2 + clientY """")
		result := RegExReplace(result, "Part-buy possible", "") ;Scrap out the part buy possible text
		result := RegexReplace(result, "o|O", "0") ;Here i use regex to fix possible reading errors from ocr
		result := RegexReplace(result, "I|l", "1")
		result := RegexReplace(result, "s|S", "5")
		result := RegExReplace(result, "[^0-9]", "")
		return result
	}
	Click() {
		MouseMove, this.X + 20 , this.Y +20
		click, right
	}
}

;Configure here the client coords for the broker window

Broker := {Tabs: {Buy: new aButton(45, 45)
				, Sell: new aButton(100, 45)
				, Sold: new aButton(180, 45)}
				
		,Rarity: {Common: new aButton(20, 75)
				, Superior: new aButton(140, 75)
				, Heroic: new aButton(260, 75)
				, Fabled: new aButton(380, 75)
				, Eternal: new aButton(500, 75)
				, Mythic: new aButton(620, 75)}
		
		,Btons: { Buy: new aButton(1150, 660)
				, Refresh: new aButton(970, 90)} ;Here place the coords for the refresh button. Use ahk window spy client coord
				
		,Search: new aButton(30,120)

		,Results: [new Item(800, 190, 920, 220) ;Here place the coords for the individual item prices. Use ahk window spy client coords
				, new Item(900, 260, 1060, 300)
				, new Item(900, 310, 1060, 350)
				, new Item(900, 360, 1060, 400)
				, new Item(900, 410, 1060, 450)
				, new Item(900, 460, 1060, 500)
				, new Item(900, 510, 1060, 550)
				, new Item(900, 560, 1060, 600)]}

;MAIN
spent := 0
items := 0

TrayTip, Press Insert to start/pause the bot and Supr to exit

pause
Insert::
pause

WinGet, aionPID, PID, A
Aion := new _ClassMemory("ahk_pid " aionPID)

gameDll := Aion.getModuleBaseAddress("Game.dll")
autodc := gameDll + 0x1489B38
autoquit := gameDll + 0x148A110
ptr := 0xB8

if !isObject(Aion)
{
	msgbox, 48, Failed to inject!, Try running the bot as administrator.
	exitapp
}

;Write values to memory to disable autodc. thx blackdraco
Aion.write(autodc, "0", type := "UInt", ptr) ;g_auto_disconnect=0
Aion.write(autoquit, "0", type := "UInt", ptr) ;g_auto_quit_yes=0


TrayTip, BrokerBot, Bot Starting!
Loop {
	Broker.Btons.Refresh.Click()
	result := Broker.Results[1].OCR()
	tooltip, Cheapest item: %result% `nKinah spent: %spent% `nItems bought: %items% `nPrice to buy: %priceToBuy%, 0, 0
	if (result <= priceToBuy && result > 0) {
		Broker.Results[1].Click()
		spent := spent + result
		items := items + 1
		sleep, 800
		Send, {Enter}
	}
sleep, Delay
}
return

Del::
ExitApp

;FUNCTIONS

JEE_ClientToScreen(hWnd, vPosX, vPosY, ByRef vPosX2, ByRef vPosY2)
{
	VarSetCapacity(POINT, 8)
	NumPut(vPosX, &POINT, 0, "Int")
	NumPut(vPosY, &POINT, 4, "Int")
	DllCall("user32\ClientToScreen", Ptr,hWnd, Ptr,&POINT)
	vPosX2 := NumGet(&POINT, 0, "Int")
	vPosY2 := NumGet(&POINT, 4, "Int")
}

StdOutToVar(cmd) {
	DllCall("CreatePipe", "PtrP", hReadPipe, "PtrP", hWritePipe, "Ptr", 0, "UInt", 0)
	DllCall("SetHandleInformation", "Ptr", hWritePipe, "UInt", 1, "UInt", 1)

	VarSetCapacity(PROCESS_INFORMATION, (A_PtrSize == 4 ? 16 : 24), 0)    ; http://goo.gl/dymEhJ
	cbSize := VarSetCapacity(STARTUPINFO, (A_PtrSize == 4 ? 68 : 104), 0) ; http://goo.gl/QiHqq9
	NumPut(cbSize, STARTUPINFO, 0, "UInt")                                ; cbSize
	NumPut(0x100, STARTUPINFO, (A_PtrSize == 4 ? 44 : 60), "UInt")        ; dwFlags
	NumPut(hWritePipe, STARTUPINFO, (A_PtrSize == 4 ? 60 : 88), "Ptr")    ; hStdOutput
	NumPut(hWritePipe, STARTUPINFO, (A_PtrSize == 4 ? 64 : 96), "Ptr")    ; hStdError
	
	if !DllCall(
	(Join Q C
		"CreateProcess",             ; http://goo.gl/9y0gw
		"Ptr",  0,                   ; lpApplicationName
		"Ptr",  &cmd,                ; lpCommandLine
		"Ptr",  0,                   ; lpProcessAttributes
		"Ptr",  0,                   ; lpThreadAttributes
		"UInt", true,                ; bInheritHandles
		"UInt", 0x08000000,          ; dwCreationFlags
		"Ptr",  0,                   ; lpEnvironment
		"Ptr",  0,                   ; lpCurrentDirectory
		"Ptr",  &STARTUPINFO,        ; lpStartupInfo
		"Ptr",  &PROCESS_INFORMATION ; lpProcessInformation
	)) {
		DllCall("CloseHandle", "Ptr", hWritePipe)
		DllCall("CloseHandle", "Ptr", hReadPipe)
		return ""
	}

	DllCall("CloseHandle", "Ptr", hWritePipe)
	VarSetCapacity(buffer, 4096, 0)
	while DllCall("ReadFile", "Ptr", hReadPipe, "Ptr", &buffer, "UInt", 4096, "UIntP", dwRead, "Ptr", 0)
		sOutput .= StrGet(&buffer, dwRead, "CP0")

	DllCall("CloseHandle", "Ptr", NumGet(PROCESS_INFORMATION, 0))         ; hProcess
	DllCall("CloseHandle", "Ptr", NumGet(PROCESS_INFORMATION, A_PtrSize)) ; hThread
	DllCall("CloseHandle", "Ptr", hReadPipe)
	return sOutput
}